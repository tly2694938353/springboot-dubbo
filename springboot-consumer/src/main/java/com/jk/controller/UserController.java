package com.jk.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.user.BootTree;
import com.jk.model.user.User;
import com.jk.service.user.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {
    @Reference
    private UserService userService;

    @RequestMapping("tolist")
    public String tolist(){
        return "redirect:queryUserList";
    }

    @RequestMapping("queryUserList")
    public String queryUserList(Model model){
        List<User> userList = userService.queryUserList();
        model.addAttribute("userList",userList);
        return "user/list";
    }

    @RequestMapping("toadd")
    public String toadd(){
        return "user/add";
    }

    @RequestMapping("saveUser")
    public String   saveUser(User user){
        userService.saveUser(user);
        return "redirect:queryUserList";
    }

    @RequestMapping("queryById")
    public String queryById(Model model,User user){
        User use = userService.queryById(user.getUserId());
        model.addAttribute("user",use);
        return "user/edit";
    }

    @RequestMapping("updateUser")
    public String updateUser(User user){
        userService.updateUser(user);
        return "redirect:queryUserList";
    }

    @RequestMapping("deleteUser")
    public String  deleteUser(Integer userId){
        userService.deleteUser(userId);
        return "redirect:queryUserList";
    }


    @RequestMapping("toindex")
    public String toindex(){
        return "user/index";
    }

    @RequestMapping("getBootTree")
    @ResponseBody
    public List<BootTree>  getBootTree(){
        List<BootTree> treelist = userService.getBootTree();
        return treelist;
    }

}
