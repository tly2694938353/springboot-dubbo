package com.jk.model.user;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class User implements Serializable {

    private Integer userId;

    private String userName;

    private String userAge;

    private String userInfo;

    private String userHobby;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date userTime;
}
