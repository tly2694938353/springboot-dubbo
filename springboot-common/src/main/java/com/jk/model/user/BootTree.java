package com.jk.model.user;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class BootTree implements Serializable {
    private Integer id;

    private String text;

    private Integer pid;

    private Boolean selectable;

    private List<BootTree> nodes;

    private String href;


}
