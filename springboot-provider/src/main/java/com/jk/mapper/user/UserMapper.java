package com.jk.mapper.user;

import com.jk.model.user.BootTree;
import com.jk.model.user.User;

import java.util.List;

public interface UserMapper {

    void saveUser(User user);

    List<User> queryUserList();

    void updateUser(User user);

    User queryById(Integer userId);

    void deleteUser(Integer userId);

    List<BootTree> getBootTreeNodes(Integer pid);
}
