package com.jk.service.user;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.user.UserMapper;
import com.jk.model.user.BootTree;
import com.jk.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass =UserService.class )
@Component
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public void saveUser(User user) {
        userMapper.saveUser(user);
    }

    @Override
    public List<User> queryUserList() {

        return  userMapper.queryUserList();
    }

    @Override
    public User queryById(Integer userId) {
        return userMapper.queryById(userId);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);
    }

    @Override
    public void deleteUser(Integer userId) {
        userMapper.deleteUser(userId);
    }

    @Override
    public List<BootTree> getBootTree() {
        int pid = -1;
        List<BootTree> bootTrees = getBootTreeNodes(pid);
        return bootTrees;
    }

    private List<BootTree> getBootTreeNodes(Integer pid) {
        List<BootTree> bootTrees = userMapper.getBootTreeNodes(pid);
        for (BootTree bootTreeBean : bootTrees) {
            Integer id2 = bootTreeBean.getId();
            List<BootTree> bootTreeNode = getBootTreeNodes(id2);
            if (bootTreeNode == null || bootTreeNode.size() <= 0) {
                bootTreeBean.setSelectable(true);
            }else {
                bootTreeBean.setSelectable(false);
                bootTreeBean.setNodes(bootTreeNode);
            }
        }
        return bootTrees;
    }
}
