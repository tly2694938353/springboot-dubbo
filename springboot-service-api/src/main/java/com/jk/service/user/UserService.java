package com.jk.service.user;

 import com.jk.model.user.BootTree;
 import com.jk.model.user.User;
 import java.util.List;

public interface UserService {

    void saveUser (User user);


    List<User> queryUserList();

    User queryById(Integer userId);

    void updateUser(User user);

    void deleteUser(Integer userId);

    List<BootTree> getBootTree();
}
